#ifndef TOOLS_H
#define TOOLS_H
#include <map>
#include <string>
#include <stdio.h>
typedef unsigned char byte;
//typedef bitset<1> bit;
static int label_counter = 0;
string itoa(int c);
map<string, byte> temps = {
  {"$t0", 0}, {"$t5", 0},
  {"$t1", 0}, {"$t6", 0},
  {"$t2", 0}, {"$t7", 0},
  {"$t3", 0}, {"$t8", 0},
  {"$t4", 0}, {"$t9", 0},
};

string newLabel(const string prefix){
  return prefix+"label"+itoa(label_counter++);
}
string newTemp(){
  map<string, byte>::iterator it = temps.begin();
  while(it != temps.end()){
    if(!it->second){
      it->second = 1;
      return it->first;
    }
    it++;
  }

  return "out of temporal registers";
}

string itoa(int c){
  char buffer[10];
  sprintf(buffer, "%d", c);
  return string(buffer);
}

void releaseTemp(string temp){
  temps[temp] = 0;
}

#endif
