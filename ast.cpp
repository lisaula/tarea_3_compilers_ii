#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include "ast.h"
#include "tools.h"

using namespace std;

map<string, int> vars;
#define DEFINE_GENCODE_EXPRESSION(name, inst)\
void name##Expr::generateCode(generated_t &ctx){\
    generated_t leftCode, rightCode;\
    expr1->generateCode(leftCode);\
    expr2->generateCode(rightCode);\
    ctx.code = leftCode.code + "\n" + rightCode.code;\
    releaseTemp(leftCode.place);\
    releaseTemp(rightCode.place);\
    ctx.place = newTemp();\
    ctx.code += "\n"#inst " "+ctx.place+", "+leftCode.place+", "+rightCode.place;\
}


int expt(int p, unsigned int q)
{
    int r = 1;

    while (q != 0) {
        if (q % 2 == 1) {    // q is odd
            r *= p;
            q--;
        }
        p *= p;
        q /= 2;
    }

    return r;
}

DEFINE_GENCODE_EXPRESSION(EQ, seq);
DEFINE_GENCODE_EXPRESSION(NE, sne);
DEFINE_GENCODE_EXPRESSION(GT, sgt);
DEFINE_GENCODE_EXPRESSION(LT, slt);
DEFINE_GENCODE_EXPRESSION(GTE, sge);
DEFINE_GENCODE_EXPRESSION(LTE, sle);
DEFINE_GENCODE_EXPRESSION(Add, add);
DEFINE_GENCODE_EXPRESSION(Sub, sub);

void MultExpr::generateCode(generated_t &ctx){
  generated_t leftCode, rightCode;
  expr1->generateCode(leftCode);
  expr2->generateCode(rightCode);
  ctx.code = leftCode.code + "\n" + rightCode.code;
  releaseTemp(leftCode.place);
  releaseTemp(rightCode.place);
  ctx.place = newTemp();
  ctx.code += "\nmove $a0, "+leftCode.place;
  ctx.code += "\nmove $a1, "+rightCode.place;
  ctx.code += "\njal mult";
  ctx.code += "\nmove "+ctx.place+", $v0";
}

void DivExpr::generateCode(generated_t &ctx){
  generated_t leftCode, rightCode;
  expr1->generateCode(leftCode);
  expr2->generateCode(rightCode);
  ctx.code = leftCode.code + "\n" + rightCode.code+"\n";
  releaseTemp(leftCode.place);
  releaseTemp(rightCode.place);
  ctx.place = newTemp();
  ctx.code += "add $sp, $sp, -8\n";
  ctx.code += "move $a0, "+leftCode.place+"\n";
  ctx.code += "move $a1, "+rightCode.place+"\n";
  ctx.code += "add $a2, $sp, $zero\n";
  ctx.code += "addi $a3, $sp, 4\n";
  ctx.code += "jal divide\n";
  ctx.code += "lw "+ctx.place+", ($sp)\n";
  ctx.code += "add $sp, $sp, 8";
}

void ModExpr::generateCode(generated_t &ctx){
  generated_t leftCode, rightCode;
  expr1->generateCode(leftCode);
  expr2->generateCode(rightCode);
  ctx.code = leftCode.code + "\n" + rightCode.code+"\n";
  releaseTemp(leftCode.place);
  releaseTemp(rightCode.place);
  ctx.place = newTemp();
  ctx.code += "add $sp, $sp, -8\n";
  ctx.code += "move $a0, "+leftCode.place+"\n";
  ctx.code += "move $a1, "+rightCode.place+"\n";
  ctx.code += "add $a2, $sp, $zero\n";
  ctx.code += "addi $a3, $sp, 4\n";
  ctx.code += "jal divide\n";
  ctx.code += "lw "+ctx.place+", 4($sp)\n";
  ctx.code += "add $sp, $sp, 8";
}


void NumExpr::generateCode(generated_t &ctx){
  ctx.place = newTemp();
  ctx.code = "li "+ctx.place+", "+itoa(value);
}

void IdExpr::generateCode(generated_t &ctx){
  ctx.place = newTemp();
  ctx.code = "lw "+ctx.place+", "+id;
}

void ExponentExpr::generateCode(generated_t &ctx){
  generated_t leftCode, rightCode;
  expr1->generateCode(leftCode);
  expr2->generateCode(rightCode);
  releaseTemp(leftCode.place);
  releaseTemp(rightCode.place);
  ctx.code = leftCode.code+"\n"+rightCode.code+"\n";
  ctx.code += "move $a0, "+leftCode.place+"\n";
  ctx.code += "move $a1, "+rightCode.place+"\n";
  ctx.code += "jal exponential\n";
  ctx.place = newTemp();
  ctx.code += "move "+ctx.place+", $v0\n";
}
void StringExpr::generateCode(generated_t &ctx){
  ctx.place = newLabel("string");
  SB->addString(ctx.place,str);
}

void InputExpr::generateCode(generated_t &ctx){
  ctx.place = newTemp();
  string linput = newLabel("input");
  ctx.code = "."+linput+": \n";
  ctx.code += "jal keypad_getkey\n";
  ctx.code += "beqz $v0, ."+linput+"\n";
  ctx.code += "move "+ctx.place+", $v0\n";
}

void CallExpr::generateCode(generated_t &ctx){
  switch (fnId) {
    case FN_TIMECLOCK:
      generateTimeClock(ctx);
      break;
    case FN_RANDINT:
      generateRandInt(ctx);
      break;
    default:
      fprintf(stderr, "Error in CallExpr\n");
  }
}

void CallExpr::generateRandInt(generated_t &ctx){
  ctx.code = "jal rand\n";
  ctx.code += "add $sp, $sp, -12\n";
  ctx.code += "sw $v0, ($sp)\n";

  generated_t limit_ctx, init_ctx;
  arg0->generateCode(init_ctx);
  ctx.code += init_ctx.code+"\n";
  ctx.place = newTemp();
  ctx.code += "lw "+ctx.place+", ($sp)\n";
  ctx.code += "add "+ctx.place+", "+ctx.place+", "+init_ctx.place+"\n";
  ctx.code += "sw "+ctx.place+", ($sp)\n";
  releaseTemp(init_ctx.place);

  arg1->generateCode(limit_ctx);
  ctx.code += limit_ctx.code+"\n";
  ctx.code += "lw $a0, ($sp)\n";
  ctx.code += "move $a1, "+limit_ctx.place+"\n";
  releaseTemp(limit_ctx.place);
  ctx.code += "addi $a2, $sp, 4\n";
  ctx.code += "addi $a3, $sp, 8\n";
  ctx.code += "jal divide\n";
  ctx.code += "lw "+ctx.place+", 8($sp)\n";
  ctx.code += "add $sp, $sp, 12\n";

}

void CallExpr::generateTimeClock(generated_t &ctx){
  ctx.place = newTemp();
  ctx.code = "lw "+ctx.place+", MS_COUNTER_REG_ADDR\n";
}

/*statements*/

string CallStatement::generateCode(){
  switch(fnId){
  case FN_RANDSEED:
    return generateRandSeed();
  default:
    fprintf(stderr, "Error in CallExpr\n");
    return "ERROR";
  }
}

string CallStatement::generateRandSeed(){
  generated_t ctx;
  arg0->generateCode(ctx);
  string my_ctx = ctx.code+"\n";
  releaseTemp(ctx.place);
  my_ctx += "move $a0, "+ctx.place+"\n";
  my_ctx += "jal rand_seed\n";
  return my_ctx;
}

string ForStatement::generateCode(){
  SB->addVariable(id);
  string ctx ="";
  generated_t start_ctx, end_ctx;
  startExpr->generateCode(start_ctx);
  ctx += start_ctx.code+"\n";
  ctx += "sw "+start_ctx.place+", "+id+"\n";
  releaseTemp(start_ctx.place);
  string lfor = newLabel("For");
  string lendfor = newLabel("EndFor");
  endExpr->generateCode(end_ctx);
  ctx += lfor+": \n";
  ctx += end_ctx.code+"\n";
  string place = newTemp();
  ctx += "lw "+place+", "+id+"\n";
  ctx += "slt "+place+", "+place+", "+end_ctx.place+"\n";
  releaseTemp(end_ctx.place);
  ctx += "beqz "+place+", "+lendfor+"\n";
  releaseTemp(place);
  ctx += block->generateCode()+"\n";
  place = newTemp();
  ctx += "lw "+place+", "+id+"\n";
  ctx += "addi "+place+", "+place+", 1\n";
  ctx += "sw "+place+", "+id+"\n";
  releaseTemp(place);
  ctx += "j "+lfor+"\n";
  ctx += lendfor+": \n";

  return ctx;
}

string WhileStatement::generateCode(){
  generated_t cond_ctx;
  cond->generateCode(cond_ctx);
  releaseTemp(cond_ctx.place);
  string lwhile = newLabel("while");
  string lend = newLabel("endWhile");
  string ctx = "";
  ctx = lwhile+": \n"+cond_ctx.code+"\n";
  ctx += "beqz "+cond_ctx.place+", "+lend+"\n";
  ctx += block->generateCode()+"\n";
  ctx += "j "+lwhile+"\n";
  ctx += lend+":\n";
  return ctx;
}

string PassStatement::generateCode(){
  return "";
}

string IfStatement::generateCode(){
  string lif = newLabel("else");
  string lend = newLabel("ifEnd");
  generated_t cond_ctx;
  cond->generateCode(cond_ctx);
  releaseTemp(cond_ctx.place);
  string ctx;
  ctx = cond_ctx.code+"\n";
  ctx += "beqz "+cond_ctx.place+", "+lif+"\n";
  ctx += trueBlock->generateCode()+"\n";
  ctx += "j "+lend+"\n";
  ctx += lif+":\n";
  if(falseBlock){
    ctx += falseBlock->generateCode()+"\n";
  }
  ctx += lend+":\n";
  return ctx;
}

string PrintStatement::generateCode(){
  string ctx = "";
  list<Expr *>::iterator it = lexpr.begin();
  while (it != lexpr.end()) {
    Expr *expr = *it;
    generated_t expr_ctx;
    expr->generateCode(expr_ctx);
    if (expr->isA(STRING_EXPR)) {
      ctx += "la $a0, "+expr_ctx.place+"\n";
      ctx += "jal puts\n";
    }else{
      ctx += expr_ctx.code+"\n";
      ctx += "move $a0, "+expr_ctx.place+"\n";
      ctx += "jal put_udecimal\n";
      releaseTemp(expr_ctx.place);
    }
    it++;
  }
  ctx += "li $a0, '\\n'\njal put_char\n";
  return ctx;
}

string AssignStatement::generateCode(){
  generated_t expr_ctx;
  expr->generateCode(expr_ctx);
  string ctx =  expr_ctx.code+"\n";
  releaseTemp(expr_ctx.place);
  ctx += "sw "+expr_ctx.place+", "+id+"\n";
  SB->addVariable(id);
  return ctx;
}

string BlockStatement::generateCode(){
  string _ctx = "";
  for(list<Statement*>::iterator it = stList.begin(); it != stList.end(); it++){
    _ctx += (*it)->generateCode();
  }
  return _ctx;
}




int InputExpr::evaluate()
{
	int result;

	cout << prompt;
	fflush(stdin);
	scanf("%d", &result);

	return result;
}

int CallExpr::evaluate()
{
    switch (fnId) {
        case FN_TIMECLOCK: return clock();
        case FN_RANDINT: {
            int start = arg0->evaluate();
            int end = arg1->evaluate();
            int range = end - start + 1;

            return (rand() % range) + start;
        }
        default:
            return 0;
    }
}

void BlockStatement::execute()
{
    list<Statement *>::iterator it = stList.begin();

    while (it != stList.end()) {
        Statement *st = *it;

        st->execute();
        it++;
    }
}

void PrintStatement::execute()
{
  list<Expr *>::iterator it = lexpr.begin();

  while (it != lexpr.end()) {
    Expr *expr = *it;

    if (expr->isA(STRING_EXPR)) {
      printf("%s", ((StringExpr*)expr)->str.c_str());
    } else {
      int result = expr->evaluate();
      printf("%d", result);
    }

    it++;
  }
  printf("\n");
}

void AssignStatement::execute()
{
    int result = expr->evaluate();
    vars[id] = result;
}

void IfStatement::execute()
{
    int result = cond->evaluate();

    if (result) {
        trueBlock->execute();
    } else if (falseBlock != 0) {
        falseBlock->execute();
    }
}

void WhileStatement::execute()
{
  int result = cond->evaluate();

  while (result) {
    block->execute();

    result = cond->evaluate();
  }
}

void ForStatement::execute()
{
	int val = startExpr->evaluate();
  	vars[id] = val;

	val = endExpr->evaluate();
	while (vars[id] < val) {
		block->execute();
		vars[id] = vars[id] + 1;
	}
}

void CallStatement::execute()
{
    switch (fnId) {
        case FN_RANDSEED: {
            int arg = arg0->evaluate();
            srand(arg);
        }
        default: {

        }
    }
}
