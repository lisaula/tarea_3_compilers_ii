#ifndef STRINGBUILDER_H
#define STRINGBUILDER_H
#include <stdio.h>
#include <string>
#include <string.h>
#include <iostream>
#include <map>
#include <vector>
using namespace std;

class StringBuilder{
public:
  StringBuilder(){
    variables = new map<string,string>;
  };
  void generateFile();
  string generateVariables();
  void addVariable(string var){
      variables->insert(pair<string,string>(var, "notstring"));
  }

  void addString(string varName, string str){
    variables->insert(pair<string,string>(varName, str));
  }

  map<string, string> *variables;
  string string_stream;
};
#endif
