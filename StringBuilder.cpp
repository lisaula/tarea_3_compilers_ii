#include "StringBuilder.h"

void StringBuilder::generateFile(){
  string file =  "#include \"screen.h\"\n";
  file += "#include \"system.h\"\n";
  file += ".global main\n";
  file += ".data\n";
  file += generateVariables();
  file += ".text\n";
  file += "main:\nli $a0, BRIGHT_WHITE\nli $a1, BLACK\njal set_color\njal clear_screen\n";
  file += string_stream + "\n";
  file += "jr $ra";
  printf("%s\n",file.c_str());
}

string StringBuilder::generateVariables(){
  map<string,string>::iterator it;
  string vars="";
  for(it = variables->begin(); it != variables->end(); it++){

    if(strcmp(it->second.c_str(), "notstring")==0)
      vars +="\t"+it->first+": .word 0\n";
    else
      vars +="\t"+it->first+": .asciz \""+it->second+"\"\n";
  }
  return vars;
}
